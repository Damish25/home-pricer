# frozen_string_literal: true

# Base class to be inherited by any model class supposed to be saved in DB
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
