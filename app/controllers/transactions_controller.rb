# frozen_string_literal: true

# Fetch and save transactions by city zip code
class TransactionsController < ApplicationController
  def index
    @city = City.find_by(postal_code: params.require(:zip_code))
    return if @city

    fetch
  end

  def fetch
    @city = City.find_or_initialize_by(postal_code: params.require(:zip_code))
    @city.fetch_transactions!
    render :index
  end
end
