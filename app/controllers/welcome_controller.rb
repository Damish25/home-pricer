# frozen_string_literal: true

# Home page controller
class WelcomeController < ApplicationController
  def index
    @cities = City.all
  end
end
