# frozen_string_literal: true

# Add new external id column for transactions
class AddRecordIdToTransactions < ActiveRecord::Migration[6.1]
  def change
    add_column :transactions, :record_id, :string
  end
end
