# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Welcome index page', type: :feature do
  scenario 'show cities' do
    visit root_path
    expect(page).to have_content("Villes synchronisées :")
  end
end