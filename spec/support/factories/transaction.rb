# frozen_string_literal: true

FactoryBot.define do
  factory :transaction do
    association :city, factory: :city
    transaction_date { '2022-02-24' }
    address { '1 rue de la paix' }
    estate_type { 'Maison' }
    total_area { '250' }
    number_of_rooms { '6' }
    value { '200000' }
  end
end
