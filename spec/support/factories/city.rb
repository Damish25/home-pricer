# frozen_string_literal: true

FactoryBot.define do
  factory :city do
    name { 'La Madeleine' }
    postal_code { '59110' }
  end
end
